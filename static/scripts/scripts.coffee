Main = (()->

  init = ()->

    ## Event listeners ##

    $(()->
      $c(document).trigger('Main.ready')
    )

    checklistItems = []

    $c(document).on 'Main.ready', ()->
      $c(document).trigger('Main.update')

    $c(document).on 'Main.update', ()->
      $c(document).find('.event-checklist').each ()->
        $(this).find('.checklist-item').each ()->
          checklistItems.push( $(this) )

      fabHide = new TimelineMax()
      fabHide.staggerTo(checklistItems, 0.2, {autoAlpha:0,y:100}, 0.05)

    $c(document).on 'click', '[data-nohref="true"]', (e)->
      e.preventDefault()

    $c(document).on 'click', '.mnav-mobile', (e)->
      e.preventDefault()
      $c('.mnav').addClass 'mnav-open'

    $c(document).on 'click', '.mnav-logo', (e)->
      if window.innerWidth < 375
        e.preventDefault()
        $c('.mnav').removeClass 'mnav-open'

    $c(document).on 'touchstart', '.gallery-section .element', (e)->
      $(this).toggleClass 'active'

    $c(document).on 'touchend', '.gallery-section .element', (e)->
      $(this).removeClass 'active'

    $c(document).on 'click', '.button', (e)->
      $(this).parents('.event-checklist').addClass('checklist-opened')
      fabHover = new TimelineMax()
      fabHover.staggerTo(checklistItems.reverse(), 0.3, {autoAlpha:1,y:0,ease:Back.easeOut.config(0.75)}, 0.05)
      return

    $c(document).on 'click', '.checklist-opened .button', (e)->
      $(this).parents('.event-checklist').removeClass('checklist-opened')
      fabLeave = new TimelineMax()
      fabLeave.staggerTo(checklistItems, 0.2, {autoAlpha:0,y:100}, 0.05)
      return

  return {
    init: init
  }

)()
